# Spring Boot GraphQL feature generator
The purpose of the program is to provide a generator for the repetitive code of spring boot businness features.

It generates files that expose a GraphQL API and a service to perform business operations. Freemarker is used for template files.

The project is still under ~~heavy~~ development so feel free to contribute or fil an issue.

## how to use ?

Run the jar without arguments and then follow the instructions. All arguments must be passed in lower case.
