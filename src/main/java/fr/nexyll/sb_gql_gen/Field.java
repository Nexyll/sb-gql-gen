package fr.nexyll.sb_gql_gen;

import lombok.Data;

@Data
public class Field {
    private String name;
    private Type type;
    private Boolean required;

    public Field(String name, Type type, Boolean required) {
        this.name = name;
        this.type = type;
        this.required = required;
    }

    public enum Type {
        STRING{
            @Override
            public String toString() {
                return "String";
            }
        },
        BOOLEAN{
            @Override
            public String toString() {
                return "Boolean";
            }
        },
        INTEGER{
            @Override
            public String toString() {
                return "int";
            }
        },
        DOUBLE{
            @Override
            public String toString() {
                return "double";
            }
        },
        LOCAL_DATE_TIME{
            @Override
            public String toString() {
                return "LocalDateTime";
            }
        },
        LOCAL_DATE{
            @Override
            public String toString() {
                return "LocalDate";
            }
        };

        public static Type fromString(String s){
            switch (s){
                case "string":
                    return STRING;
                case "bool":
                    return BOOLEAN;
                case "int":
                    return INTEGER;
                case "double":
                    return DOUBLE;
                case "localdatetime":
                    return LOCAL_DATE_TIME;
                case "localdate":
                    return LOCAL_DATE;
                default:
                    throw new RuntimeException("Unknow type");
            }
        }
    }
}
