package fr.nexyll.sb_gql_gen;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Field> fields = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        println("---- CREATE A NEW ENTITY ----");
        print(" > Package : ");
        String packageName = scanner.nextLine();
        print(" > Entity name : ");
        String entityName = scanner.nextLine();

        retrieveEntityFields(fields, scanner);

        Configuration configuration = getConfiguration();

        Map<String, Object> templateModel = generateTemplatesModels(entityName, fields, packageName);

        String entityNameCapFirst = capitalizeFirstLetter(entityName);

        renderTemplate(configuration, templateModel, "entity.ftl",entityNameCapFirst + ".java");
        renderTemplate(configuration, templateModel, "repo.ftl",entityNameCapFirst + "Repository.java");
        renderTemplate(configuration, templateModel, "service.ftl",entityNameCapFirst + "Service.java");
        renderTemplate(configuration, templateModel, "serviceInput.ftl",entityNameCapFirst + "ServiceInput.java");
        renderTemplate(configuration, templateModel, "gqlInputType.ftl",entityNameCapFirst + "Input.java");
        renderTemplate(configuration, templateModel, "mutationResolver.ftl",entityNameCapFirst + "MutationResolver.java");
        renderTemplate(configuration, templateModel, "queryResolver.ftl",entityNameCapFirst + "QueryResolver.java");
    }

    private static void retrieveEntityFields(List<Field> fields, Scanner scanner) {
        String fieldName;
        String fieldType;
        while (true) {
            println("Create a new field, type /q to stop adding new fields");
            print(" > name : ");
            fieldName = scanner.next();
            if (fieldName.equals("/Q") || fieldName.equals("/q")) break;
            newLine();
            println("Available types : string, bool, int, double, localdatetime, localdate, text");
            print(" > type: ");
            fieldType = scanner.next();
            newLine();
            print(" > Is this field required? (y/n):");
            boolean isRequired = scanner.next().equals("y");
            fields.add(new Field(fieldName, Field.Type.fromString(fieldType), isRequired));
            newLine();
        }
    }

    private static Map<String, Object> generateTemplatesModels(String arg, List<Field> fields, String packageName) {
        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("entityName", arg);
        templateModel.put("fields", fields);
        templateModel.put("packageName", packageName);
        return templateModel;
    }


    private static void renderTemplate(Configuration configuration, Map<String, Object> templateModel, String templateFileName, String fileName) {
        Template entityTemplate;
        try {
            FileWriter fileWriter = new FileWriter(fileName);
            entityTemplate = configuration.getTemplate(templateFileName);
            entityTemplate.process(templateModel, fileWriter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void newLine() {
        System.out.println();
    }

    private static void println(String fieldName) {
        System.out.println(fieldName);
    }

    private static void print(String s) {
        System.out.print(s);
    }

    private static Configuration getConfiguration() {
        Configuration configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        configuration.setClassLoaderForTemplateLoading(Main.class.getClassLoader(), "templates/");
        configuration.setDefaultEncoding("UTF-8");
        configuration.setLocale(Locale.FRANCE);
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        return configuration;
    }

    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }
}
