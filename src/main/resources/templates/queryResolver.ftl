package ${packageName};

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ${entityName?cap_first}QueryResolver implements GraphQLQueryResolver {

    @Autowired
    private ${entityName?cap_first}Service ${entityName}Service;

    public List<${entityName?cap_first}> ${entityName}s(){
        return this.${entityName}Service.findAll();
    }

    public ${entityName?cap_first} ${entityName}(Long id){
        return this.${entityName}Service.findOneOrThrow(id);
    }
}
