package ${packageName};

import lombok.Data;

@Data
public class ${entityName?cap_first}Input {
<#list fields as field>
    private ${field.getType().toString()} ${field.getName()};
</#list>
}
