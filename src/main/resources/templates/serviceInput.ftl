package ${packageName};

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class ${entityName}ServiceInput {
<#list fields as field>
    <#if field.getRequired() == true && field.getType() == "String">
    @NotBlank
    </#if>
    <#if field.getRequired() == true && field.getType() != "String">
    @NotNull
    </#if>
    private ${field.getType().toString()} ${field.getName()};
</#list>
}
