package ${packageName};

import lombok.*;
import javax.persistence.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ${entityName?cap_first} {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    <#list fields as field>
       <#if field.getRequired() == true && field.getType() == "String">
    @NotBlank
        </#if>
        <#if field.getRequired() == true && field.getType() != "String">
    @NotNull
        </#if>
    private ${field.getType().toString()} ${field.getName()};
    </#list>
}
