package ${packageName};

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ${entityName?cap_first}MutationResolver implements GraphQLMutationResolver {
    @Autowired
    private ${entityName?cap_first}Service ${entityName}Service;

    public ${entityName?cap_first} create${entityName?cap_first}(${entityName?cap_first}Input ${entityName}) {
    ${entityName?cap_first}ServiceInput ${entityName}ServiceInput = ${entityName?cap_first}ServiceInput.builder()
            <#list fields as field>
            .${field.getName()}(${entityName}.get${field.getName()?cap_first}())
            </#list>
            .build();
        return ${entityName}Service.create(${entityName}ServiceInput);
    }

    public ${entityName?cap_first} update${entityName?cap_first}(Long id, ${entityName?cap_first}Input ${entityName}Input) {
    ${entityName?cap_first}ServiceInput ${entityName}ServiceInput = ${entityName?cap_first}ServiceInput.builder()
        <#list fields as field>
        .${field.getName()}(${entityName}.get${field.getName()?cap_first}())
        </#list>
        .build();

        return ${entityName}Service.update(id, ${entityName}ServiceInput);
    }


    public Boolean delete${entityName?cap_first}(Long id) {
        return this.${entityName}Service.delete(id);
    }
}
