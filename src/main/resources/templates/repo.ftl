package ${packageName};

import org.springframework.data.jpa.repository.JpaRepository;

public interface ${entityName?cap_first}Repository extends JpaRepository<${entityName?cap_first}, Long> {
}
