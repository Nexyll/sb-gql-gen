package ${packageName};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ${entityName?cap_first}Service {
    @Autowired
    ${entityName?cap_first}Repository ${entityName}Repository;

    public ${entityName?cap_first} create(${entityName?cap_first}ServiceInput ${entityName}ServiceInput) {
        Validation.throwOnValidationError(${entityName}ServiceInput);

        ${entityName?cap_first} ${entityName} = ${entityName?cap_first}.builder()
            <#list fields as field>
            .${field.getName()}(${entityName}serviceInput.get${field.getName()?cap_first}())
            </#list>
            .build();

        return ${entityName}Repository.save(${entityName});
    }


    public ${entityName?cap_first} update(Long id, ${entityName?cap_first}ServiceInput ${entityName}ServiceInput) {
        Validation.throwOnValidationError(${entityName}ServiceInput);

        ${entityName?cap_first} ${entityName} = findOneOrThrow(id);

        <#list fields as field>
        ${entityName}.set${field.getName()?cap_first}(${entityName}serviceInput.get${field.getName()?cap_first}())
        </#list>

        return this.${entityName}Repository.save(${entityName});
    }

    public ${entityName?cap_first} delete(Long ${entityName}Id){
        this.${entityName}Repository.delete(this.findOneOrThrow(${entityName}Id));
        return true;
    }

    public List<${entityName?cap_first}> findAll(){
        return this.${entityName}Repository.findAll();
    }

    public ${entityName?cap_first} findOneOrThrow(Long id) {
        return this.${entityName}Repository.findById(id).orElseThrow(() -> new BusinessLogicException("Impossible de trouver ${entityName} avec l'id " + id));
    }
}
